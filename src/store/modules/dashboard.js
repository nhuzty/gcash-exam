import axios from "axios"

const state = {
    server_url:null,
    api_key:null,
    country_code:'',
    category_code: '',
    articles:null,
    totalResults:0,
    dplist:{
        country: null,
        category: null
    },
    isModal:false,
    detailsPage:null,
    pagination:9
}

const getters = {}

const actions = {
    search({state, commit}, payload) {
        console.log(`${state.server_url}v2/top-headlines?q=${payload.searchText}&pageSize=${state.pagination}&apiKey=${state.api_key}`)
        axios.get(`${state.server_url}v2/top-headlines?q=${payload.searchText}&pageSize=${state.pagination}&apiKey=${state.api_key}`)
        .then(response => {            
            commit("setContent", response.data)
        })
        .catch( error => {
           console.log(error)
        });
    },

    dpsearch({state, commit}, payload) {
        state.category_code = payload.category;
        state.country_code = payload.country;
        
        const result = state.dplist.country.find( ({ name }) => name === state.country_code );

        console.log(`${state.server_url}v2/top-headlines?country=${result ? result.code:''}&category=${state.category_code}&pageSize=${state.pagination}&apiKey=${state.api_key}`)
        axios.get(`${state.server_url}v2/top-headlines?country=${result ? result.code:''}&category=${state.category_code}&pageSize=${state.pagination}&apiKey=${state.api_key}`)
        .then(response => {            
            commit("setContent", response.data)
        })
        .catch( error => {
            console.log(error)
        });
    },    
}

const mutations = {
    setConfiguration(state, payload) {
        state.server_url = payload.url_server;
        state.api_key = payload.api_key;
        state.dplist.country = payload.country;
        state.dplist.category = payload.category;
    },

    setContent(state, payload){
        state.articles = payload.articles
        state.totalResults = payload.totalResults
    },

    setModal(state, payload){
        state.isModal = payload
    },

    setDetailsPage(state, payload){
        state.detailsPage = payload
    },
    
    setPageSize(state,payload){
        state.pagination = payload
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}