import Vue from 'vue'
import App from './App.vue'
import store from './store'

let config = fetch("/configs/config.json").then(resp => { return resp.json() })

const requests = [ config ];
Promise.all(requests)
  .then(results => {
    const setting = results[0];     
    store.commit("dashboard/setConfiguration", setting);    
    new Vue({
      el: '#app',
      store,
      render: h => h(App)
    }).$mount("#app")

});